package com.threeshare;

/*
 * Copyright 1997-2010 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activator implements BundleActivator {

    private static final Logger log = LoggerFactory.getLogger(Activator.class);
    private BundleContext bundleContext;
    private static Activator activator;

    public void start(BundleContext context) throws Exception {
    	this.bundleContext = context;
        activator = this;
        log.info(context.getBundle().getSymbolicName() + " started");
    }

    public void stop(BundleContext context) throws Exception {
        log.info(context.getBundle().getSymbolicName() + " stopped");
    }
    
    public Bundle[] getBundles() {
	        return bundleContext.getBundles();
	}

	public static Activator getInstance() {
	        return activator;
	}
}