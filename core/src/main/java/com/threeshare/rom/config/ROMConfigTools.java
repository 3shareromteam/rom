package com.threeshare.rom.config;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;

public class ROMConfigTools
{
	private static final Logger log = LoggerFactory.getLogger(ROMConfigManager.class);
	
	public static boolean createConfigContainerNode (String nodeName)
	{
		boolean created = false;
		
		return created;
	}
	
	
	private Node findNode(Node parentNode, String nodeName, boolean createIfNotExists)
			throws RepositoryException
		{
			Node node = null;
			if (parentNode != null)
			{
				if (parentNode.hasNode(nodeName))
				{
					node = parentNode.getNode(nodeName);
				}
				else if (createIfNotExists)
				{
					node = parentNode.addNode(nodeName, JcrConstants.NT_UNSTRUCTURED);
				}
			}
			return node;
		}
	
}
