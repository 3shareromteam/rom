package com.threeshare.rom.system.data;

import java.text.DecimalFormat;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JVMData {
	private long totalHeapMemory;
	private long usedHeapMemory;
	private long totalNonHeapMemory;
	private long usedNonHeapMemory;
	DecimalFormat numberFormat = new DecimalFormat("#.00");
	
	public long getTotalHeapMemory() {
		return this.totalHeapMemory;
	}
	
	public void setTotalHeapMemory(long totalHeapMemory) {
		this.totalHeapMemory = totalHeapMemory;
	}
	
	public long getUsedHeapMemory() {
		return this.usedHeapMemory;
	}
	
	public void setUsedHeapMemory(long usedHeapMemory) {
		this.usedHeapMemory = usedHeapMemory;
	}
	
	public long getFreeHeapMemory() {
		return this.totalHeapMemory - this.usedHeapMemory;
	}
	
	public String getPercentFreeHeapMemory() {
		final double percentFreeHeapMemory = ((double) this.usedHeapMemory / this.totalHeapMemory) * 100;
		return numberFormat.format(percentFreeHeapMemory);
	}

	public long getTotalNonHeapMemory() {
	  return this.totalNonHeapMemory;
	}
	
	public void setTotalNonHeapMemory(long totalNonHeapMemory) {
	  this.totalNonHeapMemory = totalNonHeapMemory;
	}
	
	public long getUsedNonHeapMemory() {
	  return this.usedNonHeapMemory;
	}
	
	public void setUsedNonHeapMemory(long usedNonHeapMemory) {
	  this.usedNonHeapMemory = usedNonHeapMemory;
	}
	
	public long getFreeNonHeapMemory() {
	  return this.totalNonHeapMemory - this.usedNonHeapMemory;
	}
	
	public String getPercentFreeNonHeapMemory() {
	  final double percentFreeNonHeapMemory = ((double) this.usedNonHeapMemory / this.totalNonHeapMemory) * 100.00;
	  return numberFormat.format(percentFreeNonHeapMemory);
	}
		
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		sb.append("<result>");
		sb.append("<channel>Total Heap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.totalHeapMemory + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Used Heap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.usedHeapMemory + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Free Heap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.getFreeHeapMemory() + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Percentage Heap Used</channel>");
		sb.append("<unit>Percent</unit>");
		sb.append("<value>" + this.getPercentFreeHeapMemory() + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Total NonHeap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.totalNonHeapMemory + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Used NonHeap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.usedNonHeapMemory + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Free NonHeap Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.getFreeNonHeapMemory() + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Percentage NonHeap Used</channel>");
		sb.append("<unit>Percent</unit>");
		sb.append("<value>" + this.getPercentFreeNonHeapMemory() + "</value>");
		sb.append("</result>");
		sb.append("</prtg>");
		return sb.toString();
	}

}
