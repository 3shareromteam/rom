package com.threeshare.rom.system;

public interface ROMSystem {
    public String getJVMData() throws Exception;
    public String getDiskUsageData() throws Exception;
    public String getOperatingSystemData() throws Exception;
    public String getSlingData() throws Exception;
    public String getThreadData() throws Exception;
}
