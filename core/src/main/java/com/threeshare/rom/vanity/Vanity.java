/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.vanity;

public class Vanity
{
	private String vanityTarget;
	private String vanityURI;
	private String redirectType;
	
	public Vanity() {}
	
	public Vanity(String vanityURI, String vanityTarget, String redirectType)
	{
		this.vanityURI = vanityURI;
		this.vanityTarget = vanityTarget;
		this.redirectType = redirectType;
	}
	
	public String getVanityTarget()
	{
		return this.vanityTarget;
	}
	
	public void setVanityTarget(String target)
    {
        this.vanityTarget = target;
    }
	
	public String getVanityURI()
	{
		return this.vanityURI;
	}
	
	public void setVanityURI(String vanity)
    {
		this.vanityURI = vanity;
    }
	
	public String getRedirectType()
	{
		return this.redirectType;
	}
	
	public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }
}