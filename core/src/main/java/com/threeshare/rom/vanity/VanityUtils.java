/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.vanity;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.day.cq.wcm.api.Page;
import com.threeshare.rom.search.SearchUtil;
import com.threeshare.rom.util.ROMConstants;
import com.threeshare.rom.util.ROMStringUtils;
import com.threeshare.rom.vanity.servlet.VanityServlet;

public class VanityUtils
{
	private static final Logger log = LoggerFactory.getLogger(VanityUtils.class);
	
	public static String processVanityUpdate (ResourceResolver resolver, String jsonString, String configNodePath)
	{
		String returnMessage = "";
		
		try
		{
			JSONObject jsonObj = new JSONObject(jsonString);
			VanityConfigNode configNode = VanityFactory.getConfigNode(resolver, configNodePath);
			
			if (configNode != null)
			{
				JSONArray vanityJSONArray = jsonObj.getJSONArray(ROMConstants.JSON_OBJECT_VANITY_ID);
				
				for (int i = 0; i < vanityJSONArray.length(); i++)
				{
					JSONObject vanityObj = vanityJSONArray.getJSONObject(i);
					returnMessage = returnMessage + updateVanity(resolver, configNode, vanityObj);
				}
			}
		} catch (JSONException ex) {
			
			log.error("Vanity Update Process caught a JSON Exception", ex);
		} catch (Exception ex) {
			log.error("Vanity Update Process caught an Exception", ex);
		}
		
		return returnMessage;
	}
	
	public static boolean processVanityDelete (ResourceResolver resolver, String jsonString, String configNodePath)
	{
		boolean delete = false;
		try
		{
			JSONObject jsonObj = new JSONObject(jsonString);
			VanityConfigNode configNode = VanityFactory.getConfigNode(resolver, configNodePath);
			
			if (configNode != null)
			{
				JSONArray vanityJSONArray = jsonObj.getJSONArray(ROMConstants.JSON_OBJECT_VANITY_ID);
				
				for (int i = 0; i < vanityJSONArray.length(); i++)
				{
					JSONObject vanityObj = vanityJSONArray.getJSONObject(i);
					delete = deleteVanity(resolver, configNode, vanityObj);
				}
			}
		} catch (JSONException ex) {
			log.error("Vanity Delete Process caught a JSON Exception", ex);
		} catch (Exception ex) {
			log.error("Vanity Delete Process caught an Exception", ex);
		}
		
		return delete;
	}
	
	public static boolean processVanityCheck(ResourceResolver resolver, String vanityURI, String path) {
		return processVanityCheck(resolver, vanityURI, path, null);
	}
	
	public static boolean processVanityCheck(ResourceResolver resolver, String vanityURI, String path, VanityConfigNode activeConfigNode) {
		// Default values
		String activeVanityType = ROMConstants.VANITY_TYPE_STANDARD;
		String[] activeVanityDomains = {ROMConstants.ROOT_CONTENT_PATH};
		
		// Attempt to find valid vanity configuration node if not known
		if (activeConfigNode == null)
		{
			String vanityConfigurationsPath = VanityServlet.vanityConfigurationPath;
			
			// Retrieve vanity configuration nodes
			VanityConfigNode[] configNodes = VanityFactory.getConfigNodes(resolver, vanityConfigurationsPath);
			
			// Retrieve vanity domain paths from each configNode and determine 
			// if the current vanity path resides in one of the paths.
			for (VanityConfigNode configNode : configNodes)
			{
				boolean inVanityDomain = isPathInVanityDomain(configNode, path);
				if (inVanityDomain)
				{
					log.info("Vanity config node found at " + configNode.getPath());
					activeConfigNode = configNode;
					break;
				}
			}
		}
		
		if (activeConfigNode != null)
		{
			activeVanityType = activeConfigNode.getVanityType();
			activeVanityDomains = activeConfigNode.getVanityDomain();
		}
		
		// Determine if vanity already exists in active content path
		boolean vanityAlreadyExists = vanityCheck(resolver, vanityURI, activeVanityType, activeVanityDomains, activeConfigNode);
		
		return vanityAlreadyExists;
	}
	
	public static String cleanVanityURI (String vanityUrl)
	{
		return ROMStringUtils.cleanPath(vanityUrl, true, false, false);
	}
	
	public static Vanity[] parseCustomVanityResults(
			List<Resource> vanitySearchHitResources)
	{
		return parseCustomVanityResults(vanitySearchHitResources, null);
	}
	
	public static Vanity[] parseCustomVanityResults(
			List<Resource> vanitySearchHitResources, String[] exclusionPaths)
	{

		List<Vanity> vanityList = new ArrayList<Vanity>();
		
		try
		{
			for (Resource resource : vanitySearchHitResources)
			{
		        if (resource != null)
		        {
		        	// If the jcr:content node is returned, get the parent
		        	if (resource != null && resource.getPath().endsWith(JcrConstants.JCR_CONTENT))
		        	{
		        		resource = resource.getParent();
		        	}

		            ValueMap props = ResourceUtil.getValueMap(resource);
		            String[] vanityUrlsPropArray = props.get(JcrConstants.JCR_CONTENT + "/" + ROMConstants.VANITY_TYPE_CUSTOM_JCR_PROPERTY ,String[].class);
		            Page hPage = resource.adaptTo(Page.class);
		            
		            if (hPage != null && vanityUrlsPropArray != null)
		            {
		                for(String vanityUrlsProp : vanityUrlsPropArray)
		                {
		                	JSONObject jsonProp = new JSONObject(vanityUrlsProp);
		                	String vanityURI = jsonProp.get(ROMConstants.VANITY_URI_PAGE_PROPERTY_KEY).toString();
		                	String redirectType = jsonProp.get(ROMConstants.REDIRECT_TYPE_PAGE_PROPERTY_KEY).toString();
		                	
		                	if (vanityURI != null && vanityURI.length() > 0)
		                	{
		                		// check for exclusions passed from the
		                		String path = hPage.getPath();
		                		if (!isPathInExclusionList(exclusionPaths, path))
		                		{
		                			vanityURI = cleanVanityURI(vanityURI);
		                			Vanity vanity = new Vanity(vanityURI, path, redirectType);
		                			vanityList.add(vanity);
		                		}
		                	}
		                }
		            }
		        }
			}
		}
		catch (Exception ex)
		{
			log.error("Error during parsing of custom page property vanity config.", ex);
		}
		
		Vanity[] VanityUriArray = vanityList.toArray(new Vanity [vanityList.size()]); 
		
		return VanityUriArray;
		
	}
	
	public static Vanity[] parseStandardVanityResults(
			List<Resource> vanitySearchHitResources)
	{
		return parseStandardVanityResults(vanitySearchHitResources, null);
	}
	
	public static Vanity[] parseStandardVanityResults(
			List<Resource> vanitySearchHitResources, String[] exclusionPaths) 
	{

		List<Vanity> vanityList = new ArrayList<Vanity>();
		
		try
		{
			for (Resource resource : vanitySearchHitResources)
			{
				if (resource != null)
		        {
		        	// If the jcr:content node is returned, get the parent
		        	if (resource != null && resource.getPath().endsWith(JcrConstants.JCR_CONTENT))
		        	{
		        		resource = resource.getParent();
		        	}
		        	
		            ValueMap props = ResourceUtil.getValueMap(resource);
		            String[] vanityUrls = props.get(JcrConstants.JCR_CONTENT + "/" +ROMConstants.VANITY_TYPE_STANDARD_JCR_PROPERTY, String[].class);
		            Page hPage = resource.adaptTo(Page.class);
		            if (hPage != null && vanityUrls != null)
		            {
		                for(String vanityUrl : vanityUrls)
		                {
		                	if (vanityUrl != null && vanityUrl.length() > 0)
		                	{
		                		// check for exclusions
		                		String path = hPage.getPath();
		                		if (!isPathInExclusionList(exclusionPaths, path))
		                		{
		                			vanityUrl = cleanVanityURI(vanityUrl);
		                			Vanity vanity = new Vanity(vanityUrl, path, null);
		                			vanityList.add(vanity);
		                		}
		                	}
		                }
		            }
		        }
			}
		}
		catch (Exception ex)
		{
			log.error("Error during parsing of custom page property vanity config.", ex);
		}
		
		Vanity[] VanityUriArray = vanityList.toArray(new Vanity [vanityList.size()]);
		
		return VanityUriArray;
		
	}
	
	public static Vanity[] getVanitiesInPath(ResourceResolver resolver,
			String activeVanityType, String[] activeVanityDomains, String[] exclusions) {
		
		// Determine whether to look for OOB property or custom
		String vanityTypeKey = ROMConstants.VANITY_TYPE_STANDARD_JCR_PROPERTY;
		if (activeVanityType.equals(ROMConstants.VANITY_TYPE_CUSTOM))
		{
			vanityTypeKey = ROMConstants.VANITY_TYPE_CUSTOM_JCR_PROPERTY;
		}
		
		// Search for vanities
		List<Resource> vanitySearchHitResources = SearchUtil.propertySearchResource (vanityTypeKey, "*", activeVanityDomains, resolver);
		
		// Parse search hits and remove hits that fall in the exclusion path
		Vanity[] vanityURIs = parseVanitySearchResults(activeVanityType, vanitySearchHitResources, exclusions);
		
		
		return vanityURIs;
	}

	private static boolean deleteVanity(ResourceResolver resolver,
			VanityConfigNode configNode, JSONObject vanityObj) {
		
		boolean delete = false;
		String vanityPath = "";
		
		try
		{
			vanityPath = vanityObj.getString(ROMConstants.NODE_PATH_JSON_KEY);
			Node node = resolver.resolve(vanityPath).adaptTo(Node.class);
			
			if (node != null)
			{
				node.remove();
				resolver.adaptTo(Session.class).save();
				delete=true;
			}
		}
		catch (Exception ex)
		{
			log.error("Unable to delete vanity node", ex);
			return false;
		}
		
		return delete;
	}

	private static String updateVanity(ResourceResolver resolver, VanityConfigNode configNode, 
			JSONObject vanityObj) throws Exception
	{
		String vanityURI = "";
		String target = "";
		String redirectType = "";
		String vanityPath = "";
		String returnMessage = "";
		
		try
		{
			vanityURI = vanityObj.getString(ROMConstants.VANITY_URI_JSON_KEY);
			target = vanityObj.getString(ROMConstants.TARGET_JSON_KEY);
			redirectType = vanityObj.getString(ROMConstants.REDIRECT_TYPE_JSON_KEY);
			vanityPath = vanityObj.getString(ROMConstants.NODE_PATH_JSON_KEY);
			
			String textValid = validateUpdateRequest(vanityURI, target, redirectType, vanityPath);
			
			if (textValid == null || textValid.length() == 0)
			{
				VanityNode vanity = getVanity(resolver, configNode, vanityPath);
				
				// Null vanity denotes a new vanity
				if (vanity == null)
				{
					// Check for duplicate vanities if enabled on new vanities only.
					boolean vanityAlreadyExists = false;
					if (VanityServlet.duplicateCheckEnabled)
					{
						vanityAlreadyExists = processVanityCheck(resolver, vanityURI, target, configNode);
					}
					
					if (!vanityAlreadyExists)
					{
						// Create new vanity
						configNode.createVanity(vanityURI, target, redirectType);
						log.info("Create Vanity. Vanity: " + vanityURI + ", Target: " + target);
					}
					else
					{
						returnMessage = "Vanity creation failed due to duplicate vanity. Vanity: " 
								+ vanityURI + " Target: " + target + ".";
						log.error(returnMessage);
					}
				}
				else
				{
					log.info("Update Vanity= Vanity: " + vanityURI + ", Target: " + target);
					vanity.setVanityURI(vanityURI);
					vanity.setVanityTarget(target);
					vanity.setRedirectType(redirectType);
				}
			}
			else 
			{
				returnMessage = "Vanity update/creation failed due to invalid request. " + textValid + " ";
				log.error(returnMessage);
			}
		}
		catch (Exception ex)
		{
			returnMessage = "Vanity update/creation failed for unknown reason.";
			log.error(returnMessage, ex);
		}
		
		return returnMessage;
	}

	private static boolean vanityCheck(ResourceResolver resolver, String vanityURItoCheck, 
			String activeVanityType, String[] activeVanityDomains, VanityConfigNode configNode)
	{
		boolean vanityAlreadyExists = false;
		
		String[] exclusions = null;
		if (configNode != null)
		{
			exclusions = configNode.getExclusions();
		}
	
		// Get Vanity URIs in repository 
		String[] vanityURIs = getVanitiesURIsInPath(resolver, activeVanityType, activeVanityDomains, exclusions);
		
		// Clean check vanity 
		vanityURItoCheck = cleanVanityURI(vanityURItoCheck);
		
		// Remove Duplicates
		vanityURIs = ROMStringUtils.removeDuplicates(vanityURIs);
		
		for (String vanityURI : vanityURIs)
		{
			if (!vanityAlreadyExists && vanityURI.equals(vanityURItoCheck))
			{
				vanityAlreadyExists = true;
				break;
			}
		}
	
		// Check for existing vanity in external vanity path (configNode)
		if (!vanityAlreadyExists && configNode != null)
		{
			// Get Vanity URIs
			List<String> vanityURIsInConfigNode = getVanityURIs(configNode.getVanities());
			
			// Remove Duplicates
			vanityURIsInConfigNode = ROMStringUtils.removeDuplicates(vanityURIsInConfigNode);
			
			for (String vanityURI : vanityURIsInConfigNode)
			{
				if (!vanityAlreadyExists && vanityURI.equals(vanityURItoCheck))
				{
					vanityAlreadyExists = true;
					break;
				}
			}
		}
		
		return vanityAlreadyExists;
	}

	private static String validateUpdateRequest (String vanityURI, String target, String redirectType, 
			String vanityPath)
	{
		String returnMessage = "";
		if (vanityURI == null || vanityURI.length() <= 0)
		{
			returnMessage = returnMessage + " VanityURI validation failed for null or zero length check. ";
			log.error(returnMessage);
		}
		else if (target == null || target.length() <= 0)
		{
			returnMessage = returnMessage + " Target validation failed for null or zero length check. ";
			log.error(returnMessage);
		}
		else if (redirectType == null || redirectType.length() <= 0)
		{
			returnMessage = returnMessage + " RedirectType validation failed for null or zero length check. ";
			log.error(returnMessage);
		}
		else if (vanityPath == null || vanityPath.length() <= 0)
		{
			returnMessage = returnMessage + " VanityPath validation failed for null or zero length check. ";
			log.error(returnMessage);
		}
		
		if (returnMessage.equals(""))
		{
			returnMessage = null;
		}
		
		return returnMessage;
	}
	
	
	private static VanityNode getVanity (ResourceResolver resolver, VanityConfigNode configNode, 
			String vanityPath)
	{
		VanityNode vanity = null;
		
		try
		{
			vanity = VanityFactory.getVanity(resolver, vanityPath);
		}
		catch (Exception ex)
		{
			log.error("GetVanity error detected", ex);
		}
		
		return vanity;
	}
	
	private static String[] getVanitiesURIsInPath(ResourceResolver resolver,
			String activeVanityType, String[] activeVanityDomains, String[] exclusions) {
		
		Vanity[] vanityArray = getVanitiesInPath(resolver, activeVanityType, activeVanityDomains, exclusions);
		List<String> vanityURIs = new ArrayList<String>();
		
		for (Vanity vanity : vanityArray)
		{
			String vanityURI = vanity.getVanityURI();
			if (vanityURI != null && vanityURI.length() > 0)
			{
				vanityURIs.add(vanityURI);
			}
		}
		
		String[] VanityUriArray = vanityURIs.toArray(new String [vanityURIs.size()]);
		
		return VanityUriArray;
	}
	
	private static Vanity[] parseVanitySearchResults(String activeVanityType,
			List<Resource> vanitySearchHitResources, String[] exclusions) {
		
		// Determine which parsing mechanism to use
		if (activeVanityType.equals(ROMConstants.VANITY_TYPE_CUSTOM))
		{
			return parseCustomVanityResults(vanitySearchHitResources, exclusions);
		}
		else
		{
			return parseStandardVanityResults(vanitySearchHitResources, exclusions);
		}
		
	}

	private static List<String> getVanityURIs(List<VanityNode> vanities)
	{
		List<String> vanityURIs = new ArrayList<String>();
		try
		{
			for (VanityNode vanity : vanities)
			{
				String vanityURI = vanity.getVanityURI();
				if (vanityURI != null && vanityURI.length() > 0)
				{
					vanityURI = cleanVanityURI(vanityURI);
					vanityURIs.add(vanityURI);
				}
			}
		}
		catch (Exception ex)
		{
			log.error("getVanityURIs returned an error during vanityURI processing", ex);
		}
		
		return vanityURIs;
	}

	private static boolean isPathInVanityDomain(VanityConfigNode configNode, String path)
	{
		boolean isInVanityDomain = false;
		
		String[] vanityDomains = configNode.getVanityDomain();
		
		// Validity Check
		if (vanityDomains != null && vanityDomains.length > 0)
		{
			for (String vanityDomain : vanityDomains)
			{
				// Validity Check
				if (vanityDomain != null && vanityDomain.length() > 0)
				{
					path = cleanVanityURI(path);
					vanityDomain = cleanVanityURI(vanityDomain);
					
					// If path is found in the vanity domain ensure that it is not on the exclusion list
					if (path.startsWith(vanityDomain))
					{
						if(!isPathInExclusionList(configNode, path))
						{
							isInVanityDomain = true;
						}
					}
				}
			}
		}
		
		return isInVanityDomain; 
	}

	private static boolean isPathInExclusionList(VanityConfigNode configNode, String path)
	{
		String[] exclusionPaths = configNode.getExclusions();
		return isPathInExclusionList(exclusionPaths, path);
	}
	
	private static boolean isPathInExclusionList(String[] exclusionPaths, String path)
	{
		boolean isInExclusionList = false;

		// Validity Check
		if (exclusionPaths != null)
		{
			for (String exclusionPath : exclusionPaths)
			{
				if (exclusionPath != null && exclusionPath.length() > 0)
				{
					path = cleanVanityURI(path);
					exclusionPath = cleanVanityURI(exclusionPath);
					if (path.contains(exclusionPath))
					{
						isInExclusionList = true;
					}
				}
			}
		}
		
		return isInExclusionList;
	}
	
	public static boolean isConfigNode (Node node)
	{
		boolean isConfigNode = false;
		
		if (node != null)
		{
			try 
			{
				if (node.hasProperty(ROMConstants.NODE_TYPE_PROPERTY))
				{
					if(node.getProperty(ROMConstants.NODE_TYPE_PROPERTY)
							.getString().equals(ROMConstants.VANITY_CONFIG_NODE_TYPE))
					{
						return true;
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		return isConfigNode;
	}
	
}