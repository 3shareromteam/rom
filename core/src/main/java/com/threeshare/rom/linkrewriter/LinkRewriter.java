package com.threeshare.rom.linkrewriter;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;

import com.threeshare.rom.util.ROMStringUtils;

import org.apache.sling.api.resource.Resource;
import com.day.cq.wcm.msm.api.MSMNameConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;

public class LinkRewriter {

	protected static final Logger log = LoggerFactory.getLogger(LinkRewriter.class);

	public static void rewrite(Resource resource, String oldPath, String newPath, Session session){
		try{

			Node resourceN = resource.adaptTo(Node.class);

			PropertyIterator ppi = resourceN.getProperties();

			while (ppi.hasNext()){
				Property property = ppi.nextProperty();
				String propertyName = property.getName();

				if (isSpecialProperty(propertyName)){
					continue;
				}

				if (property.isMultiple() && contains(property, oldPath)){
					Value[] oldValues = property.getValues();
					Value[] newValues = new Value[oldValues.length];
					for (int i = 0; i < newValues.length ; i++){
						Value v = oldValues[i];
						//convert relative links
						String oldValue = v.getString();
						String newValue = oldValue;
						if (oldValue != null && oldValue.startsWith(oldPath)){
							newValue = convertRelativeLink(newValue, oldPath, newPath);
							ValueFactory vf = session.getValueFactory();
							newValues[i] = vf.createValue(newValue);
						}else{
							newValues[i] = v;
						}
					}
					resourceN.setProperty(property.getName(), newValues);
				} else if (!property.isMultiple() && property.getValue().getString().contains(oldPath)){

					if (property.getName().equals("text") && property.getValue().getString().contains(oldPath)){
						//convert relative links in text
						Value oldValue = property.getValue();
						String newValue = oldValue.getString();
						if (newValue != null){
							newValue = rewriteLinksInText(newValue, oldPath, newPath);
						}
						ValueFactory vf = session.getValueFactory();//review
						resourceN.setProperty(property.getName(), vf.createValue(newValue));
						continue;
					} else{
						//convert relative link
						Value oldValue = property.getValue();
						String newValue = oldValue.getString();
						if (newValue != null && newValue.startsWith(oldPath)){
							newValue = convertRelativeLink(newValue, oldPath, newPath);
							ValueFactory vf = session.getValueFactory();
							resourceN.setProperty(property.getName(), vf.createValue(newValue));
							log.debug("Node: "+resourceN.getPath()+" with Property: "+property.getName()+" rewritten to: "+newValue);
						}
					}
				}


			}

			session.save();

		} catch (Exception e) {
			log.error("Error rewriting link properties on {}", resource.getPath(), e);
		}

	}

	public static String rewriteLinksInText(String html,String oldSiteURL, String newSiteURL){
		String result = html;
		try{
			if (ROMStringUtils.isNotBlank(html)){
				result = result.replaceAll(oldSiteURL, newSiteURL);
			}
		}
		catch(Exception e){
			log.error("Error rewriting links ", e);
		}
		return result;
	}

	public static String convertRelativeLink(String link,String oldSiteURL, String newSiteURL){
		if (ROMStringUtils.isNotBlank(link)){
			return link.replace(oldSiteURL, newSiteURL);
		}
		return link;
	}

	public static Boolean isSpecialProperty(String propertyName){
		if (propertyName.equals("sling:resourceType")
				|| propertyName.equals(NameConstants.NN_TEMPLATE)
				|| propertyName.equals(JcrConstants.JCR_PRIMARYTYPE)
				|| propertyName.equals(JcrConstants.JCR_MIXINTYPES)
				|| propertyName.equals(JcrConstants.JCR_BASEVERSION)
				|| propertyName.equals(JcrConstants.JCR_ISCHECKEDOUT)
				|| propertyName.equals(JcrConstants.JCR_PREDECESSORS)
				|| propertyName.equals(JcrConstants.JCR_UUID)
				|| propertyName.equals(JcrConstants.MIX_VERSIONABLE)
				|| propertyName.equals(JcrConstants.JCR_CREATED)
				|| propertyName.equals(JcrConstants.JCR_CREATED_BY)
				|| propertyName.equals(NameConstants.PN_PAGE_LAST_MOD)
				|| propertyName.equals(NameConstants.PN_PAGE_LAST_MOD_BY)
				|| propertyName.equals(NameConstants.PN_PAGE_LAST_REPLICATED)
				|| propertyName.equals(NameConstants.PN_PAGE_LAST_REPLICATED_BY)
				|| propertyName.equals(NameConstants.PN_PAGE_LAST_REPLICATION_ACTION)
				|| propertyName.equals(JcrConstants.JCR_VERSIONHISTORY)
				|| propertyName.equals(JcrConstants.JCR_DATA)
				|| propertyName.equals(MSMNameConstants.PN_MASTER)){
				return true;
		}
		return false;
	}

	public static Boolean contains(Property property, String oldPath){
		try{
			Value[] values = property.getValues();
			for (int i = 0; i < values.length ; i++){
				if(values[i].getString().contains(oldPath))
					return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return false;
	}

}