package com.threeshare.rom.util;

import java.text.DateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	
	private static final Logger log = LoggerFactory.getLogger(DateUtil.class);
	
	public static boolean sameDate (Calendar cal1, Calendar cal2)
	{
		boolean sameDate = false;
		try 
		{
			DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
			
			String date1 = dateFormat.format(cal1.getTime());
	        String date2 = dateFormat.format(cal2.getTime());
	        
	        if (date1.equals(date2))
	        {
	            sameDate = true;
			}
        
		}
		catch(Exception ex)
		{
			log.error("DateUtil Error: ",  ex);
		}
		
		return sameDate;
	}
}