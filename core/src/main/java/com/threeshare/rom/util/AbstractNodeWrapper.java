package com.threeshare.rom.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;

public abstract class AbstractNodeWrapper
{
	private static final Logger log = LoggerFactory.getLogger(AbstractNodeWrapper.class);

	protected ResourceResolver resolver;
	private Node node;
	private volatile int hashcode;

	public AbstractNodeWrapper(ResourceResolver resolver, Node node)
	{
		if (resolver == null || node == null)
		{
			throw new IllegalArgumentException();
		}

		this.resolver = resolver;
		this.node = node;
	}

	public Node getNode()
	{
		return node;
	}

	public ResourceResolver getResolver()
	{
		try
		{
			return resolver;
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
		
		return null;
	}
	
	public String getNodeName()
	{
		try
		{
			return getNode().getName();
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return null;
	}

	public String getPath()
	{
		String path = null;

		try
		{
			path = getNode().getPath();
		}
		catch (RepositoryException ex)
		{
			log.error(ex.toString(), ex);
		}

		return path;
	}

	public String getTitle()
	{
		String title = null;

		try
		{
			Node contentNode = getContentNode();
			if (contentNode.hasProperty(JcrConstants.JCR_TITLE))
			{
				title = contentNode.getProperty(JcrConstants.JCR_TITLE).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return title;
	}

	public void setTitle(String title)
	{
		try
		{
			Node contentNode = getContentNode();
			contentNode.setProperty(JcrConstants.JCR_TITLE, title);

			contentNode.getSession().save();
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
	}

	public String getDescription()
	{
		String description = null;

		try
		{
			Node contentNode = getContentNode();
			if (contentNode.hasProperty(JcrConstants.JCR_DESCRIPTION))
			{
				description = contentNode.getProperty(JcrConstants.JCR_DESCRIPTION).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return description;
	}

	public void setDescription(String description)
	{
		try
		{
			Node contentNode = getContentNode();
			contentNode.setProperty(JcrConstants.JCR_DESCRIPTION, description);

			contentNode.getSession().save();
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
	}

	public Date getCreated()
	{
		Date updated = null;

		try
		{
			Node theNode = getNode();
			if (theNode.hasProperty(JcrConstants.JCR_CREATED))
			{
				updated = theNode.getProperty(JcrConstants.JCR_CREATED).getDate().getTime();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return updated;
	}

	public String getCreatedBy()
	{
		String createdById = null;

		try
		{
			Node contentNode = getContentNode();
			if (contentNode.hasProperty(JcrConstants.JCR_CREATED_BY))
			{
				createdById = contentNode.getProperty(JcrConstants.JCR_CREATED_BY).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return createdById;
	}

	public Date getLastModified()
	{
		Date updated = null;

		try
		{
			Node contentNode = getContentNode();
			if (contentNode.hasProperty(JcrConstants.JCR_LASTMODIFIED))
			{
				updated = contentNode.getProperty(JcrConstants.JCR_LASTMODIFIED).getDate().getTime();
			}
			else if (contentNode.hasProperty(NameConstants.PN_PAGE_LAST_MOD))
			{
				updated = contentNode.getProperty(NameConstants.PN_PAGE_LAST_MOD).getDate().getTime();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}

		return updated;
	}

	protected Node getContentNode() throws RepositoryException
	{
		return getNode().getNode(JcrConstants.JCR_CONTENT);
	}

	protected Tag[] getAllTags()
	{
		Tag[] tags;

		TagManager tagManager = resolver.adaptTo(TagManager.class);

		try
		{
			Resource contentResource = resolver.resolve(getContentNode().getPath());
			tags = tagManager.getTags(contentResource);
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

			tags = new Tag[0];
		}

		// Keep tags sorted in alphabetical order.
		Arrays.sort(tags, new TagComparator());

		return tags;
	}

	protected void setAllTags(Tag[] tags)
	{
		TagManager tagManager = resolver.adaptTo(TagManager.class);

		// Keep tags sorted in alphabetical order.
		Arrays.sort(tags, new TagComparator());

		try
		{
			Resource contentResource = resolver.resolve(getContentNode().getPath());
			tagManager.setTags(contentResource, tags);
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
	}

	private static class TagComparator implements Comparator<Tag>
	{
		public int compare(Tag tag1, Tag tag2)
		{
			return tag1.getTitle().compareToIgnoreCase(tag2.getTitle());
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == this)
		{
			return true;
		}
		if (!(obj instanceof AbstractNodeWrapper))
		{
			return false;
		}
		AbstractNodeWrapper wrapper = (AbstractNodeWrapper) obj;
		return wrapper.getPath().equals(this.getPath());
	}

	@Override
	public int hashCode()
	{
		int result = hashcode;
		if (result == 0)
		{
			result = 76;
			result = 31 * result + this.getPath().hashCode();
			hashcode = result;
		}
		return result;
	}
}
