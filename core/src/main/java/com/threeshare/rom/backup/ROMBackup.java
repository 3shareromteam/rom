package com.threeshare.rom.backup;

public interface ROMBackup {
    public String getBackupData() throws Exception;
}
