package com.threeshare.rom.recaptcha;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;

import javax.jcr.RepositoryException;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.recaptcha.RecaptchaUtil;


@Component(metatype = true, immediate = true, label = "3|SHARE RECAPTCHA Servlet")
@Service
@Properties({
        @Property(name="service.description", value="Post captcha data to Inside site."),
        @Property(name="service.vendor", value="Inside"),
        @Property(name="sling.servlet.paths",value="/bin/RecaptchaServlet"),
        @Property(name="sling.servlet.extensions",value="html"),
        @Property(name = "sling.servlet.methods", value = {"GET"})
})
public class RecaptchaValidationServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 6844982798028020824L;
	private static Logger LOGGER = LoggerFactory.getLogger(RecaptchaValidationServlet.class);
	private static String privateKey;
	private static String publicKey;
	
	@Property (label = "Public Key", description = "RECAPTCHA Public Key", value = "")
	protected static final String RECAPTCHA_PUBLIC_KEY = "recpatcha.publickey.config";
	
	@Property (label = "Private Key", description = "RECAPTCHA Private Key", value = "")
	protected static final String CAPTCHA_PRIVATE_KEY = "recpatcha.privatekey.config";    

    @SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) throws RepositoryException {
        final Dictionary<String, Object> properties = context.getProperties();
        privateKey = PropertiesUtil.toString(properties.get(CAPTCHA_PRIVATE_KEY), "");
		publicKey = PropertiesUtil.toString(properties.get(RECAPTCHA_PUBLIC_KEY), "");
		if (publicKey != null && publicKey.length() > 0)
		{
			RecaptchaUtil.setRecaptchaPublicKey(publicKey);
		}
		else
		{
			LOGGER.error("No public RECAPTCHA key set.");
		}
	}
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException{
        LOGGER.info("");
        PrintWriter out = response.getWriter();
        out.print(checkCaptcha(request));
        out.close();
    }

    private boolean checkCaptcha(SlingHttpServletRequest request){
        LOGGER.debug("Checking Captcha...");
        String remoteAddr = request.getRemoteAddr();
        ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
        reCaptcha.setPrivateKey(privateKey);
        String challenge = request.getParameter("recaptcha_challenge_field");
        String uresponse = request.getParameter("recaptcha_response_field");
        LOGGER.debug("recaptcha_challenge_field: "+challenge);
        LOGGER.debug("recaptcha_response_field: "+uresponse);
        LOGGER.debug("Address: "+remoteAddr);
        ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
        LOGGER.info("reCaptcha is valid result: "+reCaptchaResponse.isValid());
        return reCaptchaResponse.isValid();

    }

}
