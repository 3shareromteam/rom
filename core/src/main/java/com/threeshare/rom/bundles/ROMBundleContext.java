package com.threeshare.rom.bundles;

import org.osgi.framework.BundleContext;

public class ROMBundleContext {
	private static BundleContext context;
	
	public static void setBundleContext(BundleContext bc) {
		context = bc;
	}
	
	public static BundleContext getBundleContext() {
		return context;
	}
}
