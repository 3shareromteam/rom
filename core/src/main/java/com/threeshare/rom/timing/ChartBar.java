package com.threeshare.rom.timing;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChartBar
{
	private static final Logger log = LoggerFactory.getLogger(ChartBar.class);
	
	    String input;
		String name;
		String fullname;
		int start;
		int end;
		int elapsed;
		private static final String ELLIPSIS = "...";
		private static final int MAX_LABEL_LENGTH = 25;
		
		/** Line is like
		 *  127 (2009-10-07 14:15:17) TIMER_END{97,/libs/cq/core/components/welcome/welcome.jsp#0}  
		 */
		ChartBar(String line) {
		    try {
		        input = line.trim();
		        end = Integer.valueOf(scan(' '));
		        scan('{');
		        elapsed = Integer.valueOf(scan(','));
	            start = end - elapsed;
		        fullname = cutBeforeLast(scan('}'), '#');
			    name = shortForm(fullname);
		    } catch(NumberFormatException ignored) {
		        name = fullname = ignored.toString();
		    }
		}
		 
	    /** Remove chars up to separator in this.input, and return result */
	    private String scan(char separator) {
	        final StringBuilder sb = new StringBuilder();
	        for(int i=0; i < input.length(); i++) {
	            char c = input.charAt(i);
	            if(c == separator) {
	                break;
	            }
	            sb.append(c);
	        }
	        input = input.substring(sb.length() + 1);
	        return sb.toString().trim();
	    }
	    
	    private static String cutBeforeLast(String str, char separator) {
	        int pos = str.lastIndexOf(separator);
	        if(pos > 0) {
	            str = str.substring(0, pos);
	        }
	       return str;
	    }
	    
	    private static String shortForm(String str) {
	        String result = basename(str);
	        if(result.length() > MAX_LABEL_LENGTH) {
	            result = result.substring(0, MAX_LABEL_LENGTH - ELLIPSIS.length()) + ELLIPSIS;
	        }
	        return result;
	    }
	    
	    private static String basename(String path) {
	        String result = path;
	        int pos = path.lastIndexOf('/');
	        if(pos > 0) {
	            result = result.substring(pos + 1);
	        }
	        return result;
	    }
	    	    
	    public static Comparator<ChartBar> sortChartBar = new Comparator<ChartBar>()  
	    {
		    public int compare(ChartBar a, ChartBar b) {
		        if(a.start > b.start) {
		            return 1;
		        } else if(a.start < b.start) {
		            return -1;
		        }
		        return 0;
		    }
	    };
	    
	    public String get(String key)
	    {
	    	if (key.equals("start"))
	    	{
	    		return String.valueOf(start);
	    	}
	    	else if (key.equals("end"))
	    	{
	    		return String.valueOf(end);
	    	}
	    	else if (key.equals("name"))
	    	{
	    		return name;
	    	}
	    	else
	    	{
	    		log.info("No Name found");
	    		return "";
	    	}
	    	
	    }
	    
	}