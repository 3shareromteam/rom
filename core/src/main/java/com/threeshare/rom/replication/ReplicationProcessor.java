/*
 *  Copyright 2014 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.threeshare.rom.replication;

import java.util.Dictionary;
import java.util.Map;
import java.util.Set;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentConfig;
import com.day.cq.replication.AgentManager;
import com.day.cq.replication.ReplicationAction;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

@Component(metatype = true, immediate = true, label = "3|SHARE - Replication Processor", description = "Used to replicate pages "
		+ "based on mapped path as opposed to fully qualified path. NOTE: CARE SHOULD BE TAKEN WHEN USING THIS TOOL, "
		+ "AS THE NEED FOR IT OFTEN INDICATES A DESIGN FLAW IN THE CACHING STRUCTURE.")
@Service(value = EventHandler.class)

public class ReplicationProcessor implements EventHandler {

	@Property (label = "Processing Enabled", description = "Option to enable/disable auditing. ", boolValue = false)
	protected static final String PROCESSING_ENABLED = "processing.config.enabled";
	
	@Property (label = "Event Topic", description = "Audited Event Topic", value = ReplicationAction.EVENT_TOPIC, propertyPrivate = true )
	protected static final String EVENT_TOPIC = EventConstants.EVENT_TOPIC;
	
	@Property (label = "Processed Paths", description = "Define the content paths to be handled by replication processor.", value = { "/content" , "/apps" })
	protected static final String PROCESSING_PATHS = "parocessingPaths.value.config";
	
	@Reference   
    private ResourceResolverFactory resourceResolverFactory; 
	
	@Reference
    private AgentManager agentManager;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static String eventPathProperty = "paths";
    private static boolean processingEnabled = true;
    private static String[] processingPaths;
    
    public void  handleEvent(final Event event) {
    	
    	ResourceResolver adminResolver = null;
    	
    	try
    	{
	    	if (processingEnabled) 
	    	{
	    		for (String processingPath : processingPaths)
	    		{
	    			String[] repPaths = (String[]) event.getProperty(eventPathProperty);
				    	
				    for (String repPath : repPaths)
				    {
				    	if (repPath.startsWith(processingPath))
				    	{
				    		adminResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
					    	String shortPath = adminResolver.map(repPath);
					    	if (!repPath.equals(shortPath))
					    	{
					    		log.info("Activating " + repPath + " as " + shortPath );
					    		invalidateCache(shortPath);
					    	}
				    	}
				    }
	    		}	
		    }
    	}
    	catch (Exception e)
    	{
    		log.error(e.getMessage());
    	}
    	finally
    	{
    		if (adminResolver != null)
    		{
    			adminResolver.close();
    			adminResolver = null;
    		}
    	}
    	
    }
    
    private void invalidateCache(String path) {
    	
    	// Find all replication agent
        Map<String, Agent> agentsMap = agentManager.getAgents();
        Set<String> keys = agentsMap.keySet();
        for (String key : keys)
        {
            Agent anAgent = agentsMap.get(key);
            if (anAgent.isEnabled())
            {
            	AgentConfig agentConfig = anAgent.getConfiguration();
            	log.info("Name: " + agentConfig.getName() + "; Serialization Type: " + agentConfig.getSerializationType());
            	// Only utilize flush agents that are enabled
            	if (anAgent.getConfiguration().getSerializationType().equals("flush"))
            	{
            		String hostname = agentConfig.getTransportURI();
            		if (hostname != null && hostname.length() > 0)
            		{
            			if (hostname.startsWith("http"))
            			{
            				sendInvalidation(hostname, path);
            			}
            		}
            	}
            }
        }
	}

	private void sendInvalidation(String hostname, String path) {
		String action = "Activate";
		sendDispatcherRequest(hostname, path, action);
	}
	
	private void sendDispatcherRequest(String url, String path, String action) {
		try
		{
			log.info("Sending invalidation for " + path + " to " + url);
			HttpClient client = new HttpClient();
			 
	        PostMethod post = new PostMethod(url);
	        post.setRequestHeader("CQ-Action", action);
	        post.setRequestHeader("CQ-Handle",path);
	        post.setRequestHeader("CQ-Path",path);
	         
	        StringRequestEntity body = new StringRequestEntity(path,null,null);
	        post.setRequestEntity(body);
	        post.setRequestHeader("Content-length", String.valueOf(body.getContentLength()));
	        client.executeMethod(post);
	        post.releaseConnection();
		}
    	catch (Exception e)
    	{
    		log.error(e.getMessage());
    	}
    	finally
    	{
    		
    	}
	}

	@SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) 
	{
    	// On activation, retrieve values from OSGI configuration.
    	final Dictionary<String, Object> properties = context.getProperties();
    	processingPaths = PropertiesUtil.toStringArray(properties.get(PROCESSING_PATHS));
    	processingEnabled = PropertiesUtil.toBoolean(properties.get(PROCESSING_ENABLED),true);
    }
}




