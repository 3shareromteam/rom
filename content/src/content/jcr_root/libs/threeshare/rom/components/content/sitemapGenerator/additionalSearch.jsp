<%@include file="/libs/foundation/global.jsp"%><%
%><%@ page import="com.threeshare.rom.search.SearchUtil,
    				com.threeshare.rom.util.ROMStringUtils,
					com.day.cq.wcm.api.PageManager,
                     java.util.List,
                     java.util.ArrayList,
                     java.util.Arrays,
                     java.util.Calendar,
                     java.text.SimpleDateFormat,
                     java.util.Date"%><%    


//##########################################################################
// Initialization
//########################################################################## 

String siteDomain = properties.get("sitemapconfig/siteDomain","");
String pageExtension = properties.get("sitemapconfig/pageExtension","");
String rootPagePath = properties.get("sitemapconfig/rootPagePath","");
String changeFrequency = properties.get("sitemapconfig/changeFrequency","");

String[] searchPaths = properties.get("sitemapconfig/searchPaths",String[].class);    
String[] excludePaths = properties.get("sitemapconfig/excludePaths",String[].class);
String[] excludeTags = properties.get("sitemapconfig/excludeTags",String[].class);

PageManager pm = slingRequest.getResourceResolver().adaptTo(PageManager.class);

SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

List<String> additionalPagePaths = SearchUtil.getChildPages(searchPaths, resourceResolver);
additionalPagePaths = ROMStringUtils.removeDuplicates(additionalPagePaths);

//##########################################################################
// Generate sitemap
//##########################################################################

for (String pagePath: additionalPagePaths)
{
	String lastMod = "";
    String linkPath = pagePath;
    Page linkPage = pm.getPage(pagePath);

    // handles link priority
    String linkPriority = "0.5";

    if (linkPage != null)
    {
        Calendar lastModCal = linkPage.getLastModified();
        if (lastModCal != null)
        {
            Date lastModDate = lastModCal.getTime();
            lastMod = dateFormat.format(lastModDate);
        }
    }
    
    //Exclusions
    boolean ignore = false;
    
    // Check for tag exclusions
    String[] pageTags = linkPage.getProperties().get("cq:tags",String[].class);
    if (pageTags != null && excludeTags != null)
    {
        for (String pageTag : pageTags)
        {
            if (ignore)
            {
                break;
            }
            else
            {
                for (String excludeTag : excludeTags)
                {
                    if (ignore)
                    {
                        break;
                    }
                    else
                    {
                        if (excludeTag.equals(pageTag))
                        {
                            // Exclusion found;
                            ignore = true;
                        }
                    }
                }
            }
        }
    }
    
    // Check for path exclusions
    if (excludePaths != null && !ignore)
    {
        for(String excludePath : excludePaths)
        {
            if (ignore)
            {
                break;
            }
            else
            {
                if (linkPath.contains(excludePath))
                {
                    // Exclusion found; 
                    ignore = true;
                }
            }	
        }
    }
    
    
    if (!ignore)
    {
        String cleanedLinkPath = resourceResolver.map(linkPath);
        String renderExtension = pageExtension;
        if (cleanedLinkPath.equals("/") || cleanedLinkPath.length() == 0)
        {
            renderExtension = "";
        }
        
        linkPath = siteDomain + cleanedLinkPath;
        
        %>
<url>
    <loc><%=linkPath%><%=renderExtension %></loc>
    
    <% if (lastMod != null && lastMod.length() > 0){%>
    <lastmod><%=lastMod%></lastmod>
    <%}%>
    
    <changefreq><%=changeFrequency %></changefreq>
    
    <priority><%=linkPriority%></priority>
</url><%
    }
}

%>