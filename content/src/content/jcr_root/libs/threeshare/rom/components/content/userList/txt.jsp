<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%

%><%@include file="/libs/foundation/global.jsp"%><%
%><%@ page import="com.threeshare.rom.search.SearchUtil" %><%
%><%@ page import="java.util.*" %><%
%><%@ page import="org.apache.sling.api.resource.Resource" %><%
%><%@ page import="org.apache.jackrabbit.api.security.user.*"%><%
%><%@ page import="com.threeshare.rom.util.RequestUtils" %><%
%><%

    response.setContentType("text/plain");
    Session session = resourceResolver.adaptTo(Session.class);
    boolean saveSession = "true".equals(request.getParameter("save"));
    String defaultGroupDelimiter = "<br>";
    String groupDelimiter = RequestUtils.getParameter(request,"groupDelimiter",defaultGroupDelimiter);
    String defaultSearchPath = "/home/users";
    String type = "rep:User";
    String searchPath = request.getParameter("save");
    if (searchPath == null || searchPath.length() < 1)
    {
        searchPath = defaultSearchPath;
    }
    List<Resource> searchResources = SearchUtil.searchByType(searchPath, resourceResolver, type);

    for (Resource searchResource: searchResources)
    {
		    if (searchResource != null)
        {
            Authorizable user = searchResource.adaptTo(User.class); // org.apache.jackrabbit.api.security.user.Authorizable
            if (user != null)
            {
                %><%=user.getID()%>
<%
            }
        }
    }
%>
