<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import="com.day.cq.search.*,
    com.day.cq.search.result.SearchResult, 
    com.day.cq.search.result.Hit,
    org.apache.sling.api.resource.ResourceUtil, 
    org.apache.sling.api.resource.ValueMap,
    java.util.HashMap,
    org.json.JSONObject, 
    java.util.List,
    java.util.ArrayList,
    java.util.Arrays,
    java.util.Map,
    com.threeshare.rom.util.ROMConstants,
    com.threeshare.rom.search.SearchUtil,
    org.apache.jackrabbit.JcrConstants,
    com.threeshare.rom.vanity.VanityUtils,
    com.threeshare.rom.vanity.Vanity,
    com.threeshare.rom.util.RequestUtils"%><%

    
    /*************************************************************************
     * Initializations
     *************************************************************************/
     
    String customVanityProperty = ROMConstants.VANITY_TYPE_CUSTOM_JCR_PROPERTY;
    String vanityProperty = ROMConstants.VANITY_TYPE_STANDARD_JCR_PROPERTY;
    String defaultRedirectType = ROMConstants.DEFAULT_REDIRECT_TYPE;
    String vanityURLKey = ROMConstants.VANITY_URI_PAGE_PROPERTY_KEY;
    String redirectTypeKey = ROMConstants.REDIRECT_TYPE_PAGE_PROPERTY_KEY; 
    
    
    /*************************************************************************
     * Read Query Parameters
     *************************************************************************/
     
    //Set Default Content Path
    String content = "/content";
    String contentParam = RequestUtils.getParameter(request,"content");
    content = contentParam == null ? content : contentParam;
    
    // Set Rewrite Parameters
    String rewriteParam = "";
    String rewriteQueryParam = RequestUtils.getParameter(request,"param");
    rewriteParam = rewriteQueryParam == null ? rewriteParam : rewriteQueryParam;

    //Set Remove Path, used for removing content path to match Resource Resolver rules
    String defaultRemovePath = "";
    String removePath = RequestUtils.getParameter(request,"removePath");
    removePath = removePath == null ? defaultRemovePath : removePath;

    //Set Exclusion Paths from comma separated exclude parameter
    String excludePathsParam = "";
    String excludeParam = RequestUtils.getParameter(request,"exclude");
    excludePathsParam = excludeParam == null ? excludePathsParam : excludeParam;
    
    //Set Append Path, used for append content path to match Resource Resolver rules
    String defaultAppendPath = "";
    String appendPath = RequestUtils.getParameter(request,"appendPath");
    appendPath = appendPath == null ? defaultAppendPath : appendPath;
    
    //Set Vanity type
    String vanityType = "standard";
    String customParam = RequestUtils.getParameter(request,"custom");
    vanityType = "true".equals(customParam) ? "custom" : "standard";
    
    
    /*************************************************************************
     * Find and Output Vanities
     *************************************************************************/

    response.setContentType("text/plain"); 

    %># Standard Rewrites - Page Properties<%%>

<%

    String[] exclusions = excludePathsParam.split(",");
    String[] searchPaths = content.split(",");
    Vanity[] vanitiesList = VanityUtils.getVanitiesInPath(resourceResolver, vanityType, searchPaths, exclusions);
    
    for (Vanity vanity : vanitiesList)
    {
        String vanityURI = vanity.getVanityURI();
        String vanityTarget = vanity.getVanityTarget();
        String redirectType = vanity.getRedirectType();
        String redirect = "";
        
        if (redirectType != null)
        {
            redirect = "R=" + redirectType;
        }
        
        if (vanityURI.length() > 0 && vanityURI != null)
        {
        
            if (!removePath.equals(defaultRemovePath))
            {
                vanityTarget = vanityTarget.replace(removePath,"");
            }
              
            if (!appendPath.equals(defaultAppendPath))
            {
                vanityTarget = appendPath + vanityTarget ;
            }
            
            %>RewriteRule ^<%=vanityURI %>/?$ <%=vanityTarget %>.html [<%=redirect%><%=rewriteParam %>]<%%>

<%
        
        }
    }
    
%>
