<%@ page import="org.apache.commons.lang.StringEscapeUtils,
                     com.threeshare.rom.search.SearchUtil,
                     java.util.List,
                     java.util.ArrayList"%><%
%><%@include file="/libs/foundation/global.jsp"%><%
    
    response.setContentType("text/plain");


    //TODO: Add process to automate removePaths and SearchPaths
    List<String> removePaths = new ArrayList<String>();
    removePaths.add("/content/twc");

    List<String> searchPaths = new ArrayList<String>();

    for (String searchPath : searchPaths)
    {
        List<String> exclusionPaths = SearchUtil.getChildPages(searchPath, resourceResolver);
        for (String pagePath: exclusionPaths)
        {
            %><%="Disallow: " + pagePath + ".html"%>
<%
            for (String removePath: removePaths)
            {
                if (pagePath.startsWith(removePath))
                {
                    String cleanPagePath = pagePath.replace(removePath, "");
                    if (cleanPagePath.length() > 1)
                    {
                        %><%="Disallow: " + cleanPagePath + ".html"%>
<%  
                    }
                }
            }
        }
    }
%>
