<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import=
    "com.threeshare.rom.config.ROMConfigManager,
    com.threeshare.rom.util.ROMConstants,
    com.threeshare.rom.vanity.VanityFactory,
    com.threeshare.rom.vanity.VanityConfigNode,
    com.threeshare.rom.vanity.VanityNode,
    java.util.List,
    java.util.ArrayList"%><%
    
//Set Default Content Path
String content = "/content";

// Set content from contentPath property on Node    
if (currentNode.hasProperty("contentPath"))
{
   content = currentNode.getProperty("contentPath").getString(); 
}

// Set content from query parameter 'content'
String contentOverride = request.getParameter("content");
if (contentOverride != null && !contentOverride.equals(""))
{
    content = contentOverride;
}

// Set Rewrite Parameters
String defaultRewriteParam = "";
String rewriteParam = request.getParameter("extParam");
if (rewriteParam == null || rewriteParam.equals(""))
{
    rewriteParam = defaultRewriteParam;
}

// Set Remove Path, used for removing content path to match Resource Resolver rules
String defaultRemovePath = "";
String removePath = request.getParameter("removePath");
if (removePath == null || removePath.equals(""))
{
    removePath = defaultRemovePath;
}

// Set response type
response.setContentType("text/plain");

// Configurations
String headerText = "Default Header Text";
String configPath = "";
String contentPath = "";

if (currentNode.hasProperty("extVanityTitle"))
{
    headerText = currentNode.getProperty("extVanityTitle").getString(); 
}
if (currentNode.hasProperty("configNode"))
{
    configPath = currentNode.getProperty("configNode").getString(); 
}
if (currentNode.hasProperty("contentPath"))
{
    contentPath = currentNode.getProperty("contentPath").getString(); 
}
String defaultRedirectType = "302";

// Get Vanity configuration node
VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
boolean invalidConfig = configNode == null;

// Get Vanities List from ConfigNode
List<VanityNode> vanities = invalidConfig ? new ArrayList<VanityNode>() : configNode.getVanities();

for (VanityNode vanityObj : vanities)
{
    String vanity = vanityObj.getVanityURI();
    String target = vanityObj.getVanityTarget();
    String redirectType = vanityObj.getRedirectType().equals("")
        ? defaultRedirectType : vanityObj.getRedirectType();
        
    if (vanity.length() > 0 && vanity != null)
    {
        vanity = vanity.trim();
        
        if (!vanity.startsWith("/"))
        {
            vanity = "/" + vanity;
        }
        
        if (vanity.endsWith("/"))
        {
            vanity = vanity.substring(0, vanity.length()-1);
        }
        
        if (!removePath.equals(defaultRemovePath))
        {
            target = target.replace(removePath,"");
        }
        
        // Escape % for apache rewrites
        target = target.replace("%","\\%");

        if (vanity.contains("?"))
        {
            int queryStart = vanity.indexOf("?");
			String vanityURL = vanity.substring(0,queryStart);
            String queryParamsAll = vanity.substring(queryStart + 1, vanity.length());
			String[] queryParams = queryParamsAll.split("&");
            for (String queryParam : queryParams)
            {

				%>RewriteCond %{QUERY_STRING} <%=queryParam%>
<%
            }

            vanity = vanityURL;
        }

		%>RewriteRule ^<%=vanity %>/?$ <%=target %> [R=<%=redirectType%><%=rewriteParam %>]<%%>

<%
    }

}


%>