<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.threeshare.rom.search.SearchUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.sling.api.resource.Resource" %>
<%@ page import="org.apache.jackrabbit.api.security.user.*"%>
<%@ page import="com.threeshare.rom.util.RequestUtils" %>
<%

    Session session = resourceResolver.adaptTo(Session.class);
    String defaultSearchPath = "/home/groups";
    String type = "rep:Group";
    String searchPath = request.getParameter("save");
    if (searchPath == null || searchPath.length() < 1)
    {
        searchPath = defaultSearchPath;
    }
    List<Resource> searchResources = SearchUtil.searchByType(searchPath, resourceResolver, type);

%>
    <style>
      tr:nth-child(even) {background-color: #f2f2f2}
      table { width: 100%; border-collapse: collapse; }
      th { height: 50px; }
      table, th, td { border: 1px solid black; }
      th { height: 30px; }
    </style>

    <table>
    <tr>
        <th>Group ID</th>
        <th>Group Name</th>
        <th>Group Path</th>
        <th>Member Type</th>
		<th>Member ID</th>
        <th>Member Path</th>
    </tr>
<%
    for (Resource searchResource: searchResources)
    {
		    if (searchResource != null)
        {
            Group group = searchResource.adaptTo(Group.class); // org.apache.jackrabbit.api.security.user.Authorizable
            if (group != null && group.getID() != "everyone")
            {
                String groupName = null;
                if (group.hasProperty("profile/givenName"))
                {
                	Value groupNameVal = group.getProperty("profile/givenName")[0];

                    groupName = groupNameVal != null ? groupNameVal.toString() : null;
                }

				Iterator<Authorizable> currentMembers = group.getMembers();
				if (currentMembers.hasNext())
                {
					while(currentMembers.hasNext())
                	{
                    	Authorizable member = currentMembers.next();
						String memberType = "user";
                        if (member.isGroup())
                        {
							memberType = "group";
                        }
                        %>

                        <tr>
    			    		<th><%=group.getID()%></th>
	    			      	<th><%=groupName != null ? groupName : ""%></th>
    				      	<th><%=group.getPath()%></th>
        	              	<th><%=memberType%></th>
							<th><%=member.getID()%></th>
							<th><%=member.getPath()%></th>
						</tr>
        				<%
					}
                }
                else
                {
                %>
            		<tr>
    			    	<th><%=group.getID()%></th>
    			      	<th><%=groupName != null ? groupName : ""%></th>
    			      	<th><%=group.getPath()%></th>
                      	<th></th>
						<th></th>
						<th></th>
						<th></th>  
					</tr>
        		<%
				}

                StringBuilder sb = new StringBuilder();



            }
        }
    }
%>

</table>
