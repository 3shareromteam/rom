<%@ page import="org.apache.commons.lang.StringEscapeUtils,
                     org.apache.jackrabbit.util.Text,
                     com.threeshare.rom.search.SearchUtil,
                     com.threeshare.rom.util.DateUtil,
                     com.day.cq.wcm.foundation.Sitemap,
                     com.day.cq.wcm.foundation.Sitemap.Link,
                     com.day.cq.wcm.foundation.TextFormat,
                     com.day.cq.wcm.api.PageFilter,
                     com.day.cq.wcm.api.PageManager,
                     com.day.cq.wcm.api.WCMMode,
                     java.io.IOException,
                     java.io.Writer,
                     java.io.PrintWriter,
                     java.util.Iterator,
                     java.util.Map,
                     java.util.LinkedList,
                     java.util.List,
                     java.util.ArrayList,
                     java.util.Calendar,
                     java.text.SimpleDateFormat,
                     java.util.Date,
                     com.day.cq.dam.api.Asset"%><%
%><%@include file="/libs/foundation/global.jsp"%><%

    response.setContentType("application/xml");
    

    //##########################################################################
    //Process Query Parameters
    //##########################################################################
    String defaultPath = "/content/geometrixx";
    String rootPath = request.getParameter("path");
    if (rootPath == null || rootPath.length() == 0)
    {
        rootPath = defaultPath;
    }
    
    String defaultSite = "";
    String sitePath = request.getParameter("site");
    if (sitePath == null || sitePath.length() == 0)
    {
        sitePath = defaultSite;
    }
    
    String defaultRemove = "";
    String removePath = request.getParameter("remove");
    if (removePath == null || removePath.length() == 0)
    {
        removePath = defaultRemove;
    }
    
    String defaultLevelStr = "100";
    String levelStr = request.getParameter("level");
    if (levelStr == null || levelStr.length() == 0)
    {
        levelStr = defaultLevelStr;
    }
    
    int level = Integer.parseInt(defaultLevelStr);
    try 
    {
        if (Integer.parseInt(levelStr) == (int)Integer.parseInt(levelStr))
        {
            level = Integer.parseInt(levelStr);
        }   
    }
    catch(NumberFormatException nFE) 
    {
        //Not an integer, using default value
    }
    
    String defaultLinkPriority = "0.5";
    String defaultChangeFreq = "monthly";
    String changeFreq = defaultChangeFreq;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    String[] additionalSearch = null;
    String additionalSearchParam = request.getParameter("add");
    if (additionalSearchParam != null && additionalSearchParam.length() != 0)
    {
        additionalSearch = additionalSearchParam.split(",");
    }
    
        
    //##########################################################################
    //Success Factors Exclusions
    //##########################################################################
    List<String> exclusions = new ArrayList<String>();
    exclusions.add("404");
    exclusions.add("/content/successfactors/en_us/lp/ppc");
        
    PageManager pm = slingRequest.getResourceResolver().adaptTo(PageManager.class);
    Page rootPage = pm.getPage(rootPath);

    %><?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><%
    
    //##########################################################################
    //Create Sitemap
    //##########################################################################
    if (rootPage != null)
    {
        Sitemap stm = new Sitemap(rootPage);
        LinkedList<Sitemap.Link> sitemapList = stm.getLinks();
        
        for (Sitemap.Link link : sitemapList)
        {
            if (link.getLevel() <= level)
            {   
                String lastMod = "";
                String exclude = "";
                String linkPath = link.getPath();
                Page linkPage = pm.getPage(linkPath);
    
                // handles link priority
                String linkPriority = defaultLinkPriority;
                int currentLevel = link.getLevel();
                if (currentLevel == 0 )
                {
                   linkPriority = "1.0";
                   changeFreq = "weekly";
                } 
                else if (currentLevel == 1)
                {
                    linkPriority = "0.8";
                    changeFreq = "weekly";
                }
                else
                {
                    changeFreq = defaultChangeFreq;
                }
    
                if (linkPage != null)
                {
                    ValueMap prop = linkPage.getProperties();
                    exclude = prop.get("sf:excluded", "false");
                    
                    // Get Last Modified Date
                    Calendar cal2 = linkPage.getLastModified();
                    if (cal2 != null)
                    {
                        // Hack for Google that doesnt allow sitemaps lastmod for today
                        Calendar cal1 = Calendar.getInstance();
                        String date1 = dateFormat.format(cal1.getTime());
                        String date2 = dateFormat.format(cal2.getTime());
                        
                        if (date1.equals(date2))
                        {
                            cal2.add(Calendar.DAY_OF_MONTH, -1);
                        }
                        
                        Date lastModDate = cal2.getTime();
                        lastMod = dateFormat.format(lastModDate);
                    }
                }
                
              //Exclusions
                boolean ignore = false;
                if (exclude.equals("true"))
                {
                    ignore = true;
                }
                else
                {
                    for (String exclusion: exclusions)
                    {
                        if (linkPath.contains(exclusion))
                        {
                            ignore = true;
                        }
                    }
                }
               
                if (!ignore)
                {
                    //Remove portion of path defined by 'remove' parameter 
                    //and add value of 'site' parameter
                    linkPath = sitePath + linkPath.replace(removePath,"");
                %>
        <url>
            <loc>
                <%=linkPath%>.html
            </loc>
            <% if (lastMod != null && lastMod.length() > 0){%>
            <lastmod> 
                <%=lastMod%>
            </lastmod>
            <%}%>
            <changefreq>
                <%=changeFreq %>
            </changefreq>
            <priority>
                <%=linkPriority%>
            </priority>
        </url><%
                }
            }
        }
        
        //Add any additonal search paths
        if (additionalSearch != null && additionalSearch.length != 0)
        {
            for (String searchPath: additionalSearch)
            {
                if (searchPath != null && searchPath.trim().length() != 0)
                {
                    if (!searchPath.startsWith("/"))
                    {   
                        searchPath = "/" + searchPath;
                    }
                    List<String> additionalPaths = SearchUtil.getChildPages(searchPath, resourceResolver);
                    for (String pagePath: additionalPaths)
                    {
                        String lastMod = "";
                        String linkPath = pagePath;
                        Page linkPage = pm.getPage(pagePath);
                        String exclude = "";
                        
                        if (linkPage != null)
                        {
                            ValueMap prop = linkPage.getProperties();
                            exclude = prop.get("sf:excluded", "false");
                            
                            // Get Last Modified Date
                            Calendar cal2 = linkPage.getLastModified();
                            if (cal2 != null)
                            {
                                // Hack for Google that doesnt allow sitemaps lastmod for today
                                Calendar cal1 = Calendar.getInstance();
                                String date1 = dateFormat.format(cal1.getTime());
                                String date2 = dateFormat.format(cal2.getTime());
                                
                                if (date1.equals(date2))
                                {
                                    cal2.add(Calendar.DAY_OF_MONTH, -1);
                                }
                                
                                Date lastModDate = cal2.getTime();
                                lastMod = dateFormat.format(lastModDate);
                            }
                        }
                        
                        //Exclusions
                        boolean ignore = false;
                        if (exclude.equals("true"))
                        {
                            ignore = true;
                        }
                        else
                        {
                            for (String exclusion: exclusions)
                            {
                                if (linkPath.contains(exclusion))
                                {
                                    ignore = true;
                                }
                            }
                        }
                        if (!ignore)
                        {
                            //Remove portion of path defined by 'remove' parameter 
                            //and add value of 'site' parameter
                            linkPath = sitePath + linkPath.replace(removePath,"");
                        %>
    <url>
        <loc>
            <%=linkPath%>.html
        </loc>
        <% if (lastMod != null && lastMod.length() > 0){%>
        
        <lastmod> 
            <%=lastMod%>
        </lastmod>
        
        <%}%>
        <changefreq>
            <%=defaultChangeFreq %>
        </changefreq>
        <priority>
            <%=defaultLinkPriority%>
        </priority>
    </url><%
                        }
                    }
                
                }
            }
        }
        
        //##########################################################################
        //Success Factors Customization
        //##########################################################################
        
        String damPagePrefix = "http://www.successfactors.com/en_us/download.html?a=";
        
        List<String> damAssetsInclusionPaths = new ArrayList<String>();
        damAssetsInclusionPaths.add("/content/dam/successfactors/en_us/resources/brochures");
        damAssetsInclusionPaths.add("/content/dam/successfactors/en_us/resources/brochures-product");
        damAssetsInclusionPaths.add("/content/dam/successfactors/en_us/resources/case-studies");
        damAssetsInclusionPaths.add("/content/dam/successfactors/en_us/resources/white-papers");
        
        for (String damAssetsInclusionPath : damAssetsInclusionPaths)
        {
            List<String> damAssetPaths = SearchUtil.getDAMAssets(damAssetsInclusionPath, resourceResolver);
            for (String damAssetPath : damAssetPaths)
            {
            
                Asset damAsset = resourceResolver.getResource(damAssetPath).adaptTo(Asset.class);
                String exclude = damAsset.getMetadataValue("sf:excluded");
                
                if (exclude.equals("true"))
                {
                    //ignore this link
                }
                else
                {
                
                %>
<url>
    <loc>
        <%=damPagePrefix + damAssetPath%>
    </loc>
    <changefreq>
        <%=defaultChangeFreq %>
    </changefreq>
    <priority>
        <%=defaultLinkPriority%>
    </priority>
</url><%

                }
            }
        }
    }
%>
</urlset>