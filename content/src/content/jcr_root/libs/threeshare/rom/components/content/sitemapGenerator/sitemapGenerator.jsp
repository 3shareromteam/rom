<%@include file="/libs/foundation/global.jsp"%><%

%><%@ page import="org.apache.commons.lang.StringEscapeUtils,
                     org.apache.jackrabbit.util.Text,
                     com.threeshare.rom.search.SearchUtil,
					 com.threeshare.rom.util.ROMStringUtils,
                     com.day.cq.wcm.foundation.Sitemap,
					 com.day.cq.tagging.Tag,
					 com.day.cq.tagging.TagManager,
                     com.day.cq.wcm.foundation.Sitemap.Link,
                     com.day.cq.wcm.foundation.TextFormat,
                     com.day.cq.wcm.api.PageFilter,
                     com.day.cq.wcm.api.PageManager,
                     com.day.cq.wcm.api.WCMMode,
                     java.io.IOException,
                     java.io.Writer,
                     java.io.PrintWriter,
                     java.util.Iterator,
                     java.util.Map,
                     java.util.LinkedList,
                     java.util.List,
                     java.util.ArrayList,
                     java.util.Arrays,
                     java.util.Calendar,
                     java.text.SimpleDateFormat,
                     java.util.Date"%><%    


//##########################################################################
// Initialization
//##########################################################################    

Resource configResource = resourceResolver.getResource(resource.getPath() + "/sitemapconfig");                         

String siteDomain = properties.get("sitemapconfig/siteDomain","");
String pageExtension = properties.get("sitemapconfig/pageExtension","");
String rootPagePath = properties.get("sitemapconfig/rootPagePath","");
String changeFrequency = properties.get("sitemapconfig/changeFrequency","");
String[] searchPaths = properties.get("sitemapconfig/searchPaths",String[].class);
String[] damSearchPaths = properties.get("sitemapconfig/damSearchPaths",String[].class);
String[] excludePaths = properties.get("sitemapconfig/excludePaths",String[].class);
String[] excludeTags = properties.get("sitemapconfig/excludeTags",String[].class);
String maxLevelsProp = properties.get("sitemapconfig/maxLevels","100");

SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

int maxLevel = 100;
if (ROMStringUtils.isInteger(maxLevelsProp))
{
	maxLevel = Integer.parseInt(maxLevelsProp);
}


PageManager pm = slingRequest.getResourceResolver().adaptTo(PageManager.class);
Page rootPage = pm.getPage(rootPagePath);

//##########################################################################
// Generate sitemap
//########################################################################## 

response.setContentType("application/xml");

%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><%

    if (rootPage != null)
    {
        Sitemap stm = new Sitemap(rootPage);
        LinkedList<Sitemap.Link> sitemapList = stm.getLinks();
        
        for (Sitemap.Link link : sitemapList)
        {
            if (link.getLevel() <= maxLevel)
            {   
                String lastMod = "";
                String linkPath = link.getPath();
                Page linkPage = pm.getPage(linkPath);
    
                // handles link priority
                String linkPriority = "0.5";
                int currentLevel = link.getLevel();
                if (currentLevel == 0 )
                {
                   linkPriority = "1.0";
                } 
                else if (currentLevel == 1)
                {
                    linkPriority = "0.8";
                }
    
                if (linkPage != null)
                {
                    Calendar lastModCal = linkPage.getLastModified();
                    if (lastModCal != null)
                    {
                        Date lastModDate = lastModCal.getTime();
                        lastMod = dateFormat.format(lastModDate);
                    }
                }
                
                //Exclusions
                boolean ignore = false;

                // Check for tag exclusions
	            String[] pageTags = linkPage.getProperties().get("cq:tags",String[].class);
				if (pageTags != null && excludeTags != null)
                {
                    for (String pageTag : pageTags)
                    {
						if (ignore)
                        {
							break;
                        }
                        else
                        {
                            for (String excludeTag : excludeTags)
                            {
                                if (ignore)
                                {
                                    break;
                                }
                                else
                                {
                                    if (excludeTag.equals(pageTag))
                                    {
                                        // Exclusion found;
                                        ignore = true;
                                    }
                                }
                            }
                        }
                    }
                }

                // Check for path exclusions
				if (excludePaths != null && !ignore)
                {
                    for(String excludePath : excludePaths)
                    {
                        if (ignore)
                        {
							break;
                        }
                        else
                        {
                            if (linkPath.contains(excludePath))
                            {
                                // Exclusion found; 
                                ignore = true;
                            }
                    	}	
                    }
                }


                if (!ignore)
                {
                    String cleanedLinkPath = resourceResolver.map(linkPath);
                    String renderExtension = pageExtension;
                    if (cleanedLinkPath.equals("/") || cleanedLinkPath.length() == 0)
                    {
                        renderExtension = "";
                    }
                    
                    linkPath = siteDomain + cleanedLinkPath;
                    
                    %>
        <url>
            <loc><%=linkPath%><%=renderExtension %></loc>

            <% if (lastMod != null && lastMod.length() > 0){%>
			<lastmod><%=lastMod%></lastmod>
        	<%}%>
            
            <changefreq><%=changeFrequency %></changefreq>
            
            <priority><%=linkPriority%></priority>
        </url><%
                }
            }
        }
    }

%>
<cq:include script="/libs/threeshare/rom/components/content/sitemapGenerator/additionalSearch.jsp"/> 
<cq:include script="/libs/threeshare/rom/components/content/sitemapGenerator/damAssets.jsp"/>

</urlset>